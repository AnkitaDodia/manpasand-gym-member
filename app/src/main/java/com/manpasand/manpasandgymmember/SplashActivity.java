package com.manpasand.manpasandgymmember;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.manpasand.manpasandgymmember.common.MasterActivity;

/**
 * Created by My 7 on 27-Jan-18.
 */

public class SplashActivity extends MasterActivity
{
    private static int SPLASH_TIME_OUT = 3000;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ctx = this;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

//                Toast.makeText(ctx,""+getIp(),Toast.LENGTH_LONG).show();
                if (!getIp().equalsIgnoreCase("NULL")) {
                    Intent i = new Intent(ctx, MemberDetailsActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(ctx, ConnectActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
