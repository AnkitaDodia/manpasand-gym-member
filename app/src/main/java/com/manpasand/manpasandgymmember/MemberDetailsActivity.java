package com.manpasand.manpasandgymmember;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.manpasand.manpasandgymmember.common.MasterActivity;
import com.manpasand.manpasandgymmember.helpers.PointDetailsAdapter;
import com.manpasand.manpasandgymmember.helpers.PointDetailsList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class MemberDetailsActivity extends MasterActivity {

    Context ctx;
    ListView list_pointsDetails;

    ImageView img_member;
    TextView txt_memberid, txt_firstname, txt_balance_tag, txt_balance, txt_title_point, txt_title_status, txt_title_datetime;

    private ArrayList<PointDetailsList> mPointDetailsList = new ArrayList<>();

    Socket socket;

    private Boolean isConnected = true;

    String memberNumber,photo,name,balance;

    PointDetailsList list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makefullscreen();
        setContentView(R.layout.activity_member_details);
        ctx = this;

        InitView();
        setTypeFace();

        sendStatusPost mTask = new sendStatusPost();
        mTask.execute();

        String event = "mapasand-channel-"+getIMEINumber()+":App\\Events\\MemberUpdate";
        Log.e("EVENT",event);

        String url = "http://"+getIp()+":8122";
        Log.e("URL",url);

        try {
            socket = IO.socket(url);
            socket.on(Socket.EVENT_CONNECT,onConnect);
            socket.on(Socket.EVENT_DISCONNECT,onDisconnect);
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.on(event, onNewMessage);
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void InitView(){

        list_pointsDetails = (ListView)findViewById(R.id.list_pointsdetails);

        img_member = (ImageView)findViewById(R.id.img_member);

        txt_memberid = (TextView)findViewById(R.id.txt_memberid);
        txt_firstname = (TextView)findViewById(R.id.txt_firstname);
        txt_balance_tag = (TextView)findViewById(R.id.txt_balance_tag);

        txt_balance  = (TextView)findViewById(R.id.txt_balance);

        txt_title_point  = (TextView)findViewById(R.id.txt_title_point);
        txt_title_status  = (TextView)findViewById(R.id.txt_title_status);
        txt_title_datetime = (TextView)findViewById(R.id.txt_title_datetime);

    }

    private void setTypeFace()
    {
        txt_memberid.setTypeface(getBoldTypeFace());
        txt_firstname.setTypeface(getGujaratiTypeFace());
        txt_balance_tag.setTypeface(getBoldTypeFace());

        txt_balance.setTypeface(getBoldTypeFace());

        txt_title_point.setTypeface(getBoldTypeFace());
        txt_title_status.setTypeface(getBoldTypeFace());
        txt_title_datetime.setTypeface(getBoldTypeFace());
    }

    private class sendStatusPost extends AsyncTask<String, String, String>
    {
        private String result;
        @Override
        protected String doInBackground(String... strings)
        {
            try {
                String Url = "http://"+getIp()+"/api/member/status";

                URL url = new URL(Url);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("unique_key", getIMEINumber());

//                Log.i("PARAMS", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

//                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
//                Log.i("MSG" , conn.getResponseMessage());

                StringBuffer sb = new StringBuffer();
                InputStream is = null;

                is = new BufferedInputStream(conn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String inputLine = "";
                while ((inputLine = br.readLine()) != null) {
                    sb.append(inputLine);
                }
                result = sb.toString();
                conn.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e("STATUS_RESPONSE",""+s);
            parseStatusData(s);
        }
    }

    private void parseStatusData(String result)
    {
        try {
            JSONObject jsonObject = new JSONObject(result);

            String status = jsonObject.getString("status");

            if(status.equalsIgnoreCase("true"))
            {
                if(jsonObject.has("message"))
                {
                    Toast.makeText(ctx,""+jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                }

                JSONObject data = (JSONObject) jsonObject.get("data");

                parseDataFromStatus(data);
            }
            else
            {
                Toast.makeText(ctx,""+jsonObject.getString("message"),Toast.LENGTH_LONG).show();
//                Log.e("message",""+jsonObject.getString("message"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setTitleData() {
        txt_memberid.setText(memberNumber);
        txt_firstname.setText(name);
        txt_balance.setText(balance);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("SOCKET", "diconnected");
                    isConnected = false;
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("SOCKET", "Error connecting");
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e("DATA",""+data.toString());
                    parseData(data);
                }
            });
        }
    };

    private void parseDataFromStatus(JSONObject data)
    {
        try {
            memberNumber = data.getString("member_number");
            photo = data.getString("photo");
            name = data.getString("name");
            balance = data.getString("balance");

            setTitleData();

            Glide.with(ctx).load(photo).placeholder(R.drawable.ic_launcher_background).into(img_member);

            JSONArray jsonArray = data.getJSONArray("data");

            for(int i = 0; i < jsonArray.length(); i++)
            {
                list = new PointDetailsList();

                list.setDatetime(jsonArray.getJSONObject(i).getString("time"));
                list.setPoints(jsonArray.getJSONObject(i).getString("point"));
                list.setStatus(jsonArray.getJSONObject(i).getString("type"));
            }

            mPointDetailsList.add(list);

            setData();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void parseData(JSONObject data)
    {
        try {
            JSONObject dataFromJson = data.getJSONObject("data");

            JSONObject member = dataFromJson.getJSONObject("member");

            memberNumber = member.getString("member_number");
            photo = member.getString("photo");
            name = member.getString("name");
            balance = member.getString("balance");

            setTitleData();

            JSONArray jsonArray = member.getJSONArray("data");

            for(int i = 0; i < jsonArray.length(); i++)
            {
                list = new PointDetailsList();

                list.setDatetime(jsonArray.getJSONObject(i).getString("time"));
                list.setPoints(jsonArray.getJSONObject(i).getString("point"));
                list.setStatus(jsonArray.getJSONObject(i).getString("type"));
                mPointDetailsList.add(list);
            }

            setData();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setData()
    {
        PointDetailsAdapter adapter = new PointDetailsAdapter(MemberDetailsActivity.this,mPointDetailsList);
        list_pointsDetails.setAdapter(adapter);
    }
}
