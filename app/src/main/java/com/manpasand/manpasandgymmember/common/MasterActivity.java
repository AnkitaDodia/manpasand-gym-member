package com.manpasand.manpasandgymmember.common;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;


public class MasterActivity extends AppCompatActivity
{
    private Typeface font;

    public static final int REQUEST_READ_PHONE_STATE = 1;

    public void makefullscreen(){

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        getSupportActionBar().hide();
    }

    // Code for font type
    public Typeface getBoldTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/ProximaNova-Bold.otf");
        return font;
    }

    public Typeface getRegularTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        return font;
    }

    public Typeface getGujaratiTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/SHRUTI.TTF");
        return font;
    }
    // Code for font type

    public void saveIp(String ip)
    {
        SharedPreferences sp = getSharedPreferences("IP_ADDRESS",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("IP",ip);
        spe.apply();
    }

    public String getIp()
    {
        SharedPreferences sp = getSharedPreferences("IP_ADDRESS",MODE_PRIVATE);
        String ip = sp.getString("IP","NULL");
        return ip;
    }

    public String getIMEINumber() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            return TODO;
        }
        String IMEINumber = mngr.getDeviceId();

//        Log.e("IMEI_NUMBER" , IMEINumber);
        return IMEINumber;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    getIMEINumber();
                }
                break;

            default:
                break;
        }
    }
}
