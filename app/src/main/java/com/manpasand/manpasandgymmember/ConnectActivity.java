package com.manpasand.manpasandgymmember;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.manpasand.manpasandgymmember.common.MasterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class ConnectActivity extends MasterActivity
{
    Context ctx;

    Button btn_connect;

    EditText edt_devicename, edt_endpointip;

    TextView txt_devicename, txt_endpointip;

    String deviceName,ip,IMEI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        ctx = this;

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        }

        InitViews();
        setTypeFace();
        setData();
    }

    public void InitViews(){

        btn_connect = (Button)findViewById(R.id.btn_connect);

        edt_devicename = (EditText)findViewById(R.id.edt_devicename);
        edt_endpointip = (EditText)findViewById(R.id.edt_endpointip);

        txt_devicename = (TextView)findViewById(R.id.txt_devicename);
        txt_endpointip = (TextView)findViewById(R.id.txt_endpointip);
    }

    private void setTypeFace()
    {
        edt_devicename.setTypeface(getBoldTypeFace());
        edt_endpointip.setTypeface(getBoldTypeFace());

        txt_devicename.setTypeface(getBoldTypeFace());
        txt_endpointip.setTypeface(getBoldTypeFace());
    }

    private void setData()
    {
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt_devicename.getText().toString().length() == 0)
                {
                    Toast.makeText(ctx,"Please enter Device name",Toast.LENGTH_LONG).show();
                }
                else if(edt_endpointip.getText().toString().length() == 0)
                {
                    Toast.makeText(ctx,"Please enter Ip Address",Toast.LENGTH_LONG).show();
                }
                else
                {
                    deviceName = edt_devicename.getText().toString();
                    ip = edt_endpointip.getText().toString();
                    IMEI = getIMEINumber();

                    sendData mData = new sendData();
                    mData.execute();
                }
            }
        });
    }

    private class sendData extends AsyncTask<String, String, String> {

        private String result;

        @Override
        protected String doInBackground(String... params) {
            try {
                String Url = "http://"+ip+"/api/device/register";
                Log.e("URL",""+Url);

                URL url = new URL(Url);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("name", deviceName);
                jsonParam.put("ip", ip);
                jsonParam.put("unique_key", IMEI);

                Log.i("JSON", jsonParam.toString());
                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG" , ""+conn.getResponseMessage());

                StringBuffer sb = new StringBuffer();
                InputStream is = null;

                is = new BufferedInputStream(conn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String inputLine = "";
                while ((inputLine = br.readLine()) != null) {
                    sb.append(inputLine);
                }
                result = sb.toString();
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            Log.e("RESULT",result);
            parseData(result);
        }
    }

    private void parseData(String result)
    {
        try {
            JSONObject jsonObject = new JSONObject(result);

            String status = jsonObject.getString("status");

            if(status.equalsIgnoreCase("false"))
            {
                Toast.makeText(ctx,""+jsonObject.getString("message"),Toast.LENGTH_LONG).show();
            }
            else
            {
                saveIp(ip);

                Intent i = new Intent(ctx, MemberDetailsActivity.class);
                startActivity(i);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
