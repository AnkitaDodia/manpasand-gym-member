package com.manpasand.manpasandgymmember.helpers;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manpasand.manpasandgymmember.MemberDetailsActivity;
import com.manpasand.manpasandgymmember.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Raavan on 02-Jan-18.
 */

public class PointDetailsAdapter extends BaseAdapter {

    ArrayList<PointDetailsList> mPointDetailsList = new ArrayList<>();

    MemberDetailsActivity mContext;

    PointDetailsHolder holder;

    public PointDetailsAdapter(MemberDetailsActivity context, ArrayList<PointDetailsList> PointDetailsList)
    {
        mContext = context;
        mPointDetailsList = PointDetailsList;
    }

    @Override
    public int getCount() {
        return mPointDetailsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPointDetailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_points_details, null);
            holder = new PointDetailsHolder();

            holder.txt_row_point = (TextView) convertView.findViewById(R.id.txt_row_point);
            holder.txt_row_status = (TextView) convertView.findViewById(R.id.txt_row_status);
            holder.txt_row_datetime = (TextView) convertView.findViewById(R.id.txt_row_datetime);


            convertView.setTag(holder);
        } else {

            holder = (PointDetailsHolder) convertView.getTag();
        }

        final PointDetailsList mPointDetailsitem = mPointDetailsList.get(i);

        holder.txt_row_point.setTypeface(mContext.getBoldTypeFace());
        holder.txt_row_status.setTypeface(mContext.getBoldTypeFace());
        holder.txt_row_datetime.setTypeface(mContext.getRegularTypeFace());

        holder.txt_row_point.setText(mPointDetailsitem.getPoints());
        holder.txt_row_datetime.setText(mPointDetailsitem.getDatetime());

        if(mPointDetailsitem.getStatus().equalsIgnoreCase("p"))
        {
            holder.txt_row_status.setText("+");
        }
        else
        {
            holder.txt_row_status.setText("-");
        }

        return convertView;
    }

    class PointDetailsHolder {

        TextView txt_row_point, txt_row_status, txt_row_datetime;
    }
}
