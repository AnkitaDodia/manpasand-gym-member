package com.manpasand.manpasandgymmember.helpers;

/**
 * Created by Raavan on 02-Jan-18.
 */

public class PointDetailsList {
    String points;
    String status;
    String datetime;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
